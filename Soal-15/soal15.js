let warna = ["biru", "merah", "kuning", "hijau"];

let dataBukuTambahan = {
    penulis: "John Doe",
    tahunTerbit: 2020
}

let buku = {
    nama: "Pemrograman Dasar",
    jumlahHalaman: 172,
    warnaSampul: ["hitam"]
}

buku = {...buku, ...dataBukuTambahan};
buku.warnaSampul = [...buku.warnaSampul, ...warna];

console.log(buku);