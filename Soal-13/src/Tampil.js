import React from 'react';

const data = [
    {name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
    {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
    {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
    {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
]

class Data extends React.Component
{
    render()
    {
        const cardname = 
        {
            border: "1px solid",
            borderRadius: "20px",
            width: "200px",
            textAlign: "left",
            marginTop: "20px",
        }

        const photo = 
        {
            
        }

        return (
            <>
                <div style={cardname} >
                    {/* <div> { call } <img src={"{this.props.photo}"} ></img></div> */}
                    <img src={this.props.photo} ></img>
                    <p> {this.props.call} <b> {this.props.name} </b> </p>
                    <p> {this.props.profession} </p>
                    <p> {this.props.age} </p>
                </div>
            </>
        );
    }
}

class DataTampil extends React.Component
{
    render()
    {
        return (
            <div>
                <h1>Hello</h1>

                {
                    data.map( (el) => {
                        return (
                            <Data 
                                photo={el.photo} 
                                call={
                                    el.gender == "Male" ? "Mr. " : "Mrs. "
                                } 
                                name={el.name} 
                                profession={el.profession} 
                                age={el.age} />
                        );
                    })
                }
            </div>
        );
        
    }
}

export default DataTampil;