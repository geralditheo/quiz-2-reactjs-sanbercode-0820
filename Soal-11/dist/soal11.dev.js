"use strict";

function filterBooksPromise(colorful, amountOfPage) {
  return new Promise(function (resolve, reject) {
    var books = [{
      name: "Shinchan",
      totalPage: 50,
      isColorful: true
    }, {
      name: "Kalkulus",
      totalPage: 250,
      isColorful: false
    }, {
      name: "Doraemon",
      totalPage: 40,
      isColorful: true
    }, {
      name: "Algoritma",
      totalPage: 300,
      isColorful: false
    }];

    if (amountOfPage > 0) {
      resolve(books.filter(function (x) {
        return x.totalPage >= amountOfPage && x.isColorful == colorful;
      }));
    } else {
      var reason = new Error("Maaf Parameter Salah");
      reject(reason);
    }
  });
}

var colorful = true;
var amountOfPage = 100;

var inputFilterBooks = function inputFilterBooks() {
  filterBooksPromise(colorful, amountOfPage).then(function (result) {
    console.log(result);
  })["catch"](function (err) {
    console.log(err + " err");
  });
};

inputFilterBooks();