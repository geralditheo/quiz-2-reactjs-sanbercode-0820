function filterBooksPromise (colorful, amountOfPage)
{
    return new Promise(function(resolve, reject) {
        
        let books = 
        [
            {name: "Shinchan", totalPage: 50, isColorful: true},
            {name: "Kalkulus", totalPage: 250, isColorful: false},
            {name: "Doraemon", totalPage: 40, isColorful: true},
            {name: "Algoritma", totalPage: 300, isColorful: false}
        ]

        if (amountOfPage > 0)
        {
            resolve(books.filter( (x) => {
                return x.totalPage >= amountOfPage && x.isColorful == colorful;
            }));
        }
        else
        {
            let reason = new Error("Maaf Parameter Salah");
            reject(reason);
        }
    });
}

let colorful = true;
let amountOfPage = 100;

let inputFilterBooks = () => 
{
    filterBooksPromise(colorful, amountOfPage)
        .then((result) => {
            console.log(result);
        }).catch((err) => {
            console.log(err + " err");
        });
}

inputFilterBooks();