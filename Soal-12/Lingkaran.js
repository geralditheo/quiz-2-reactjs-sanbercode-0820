class Lingkaran
{
    constructor(r)
    {
        this._radius = r;
        this._phi = 3.14;
    }
    get radius()
    {
        return this._radius;
    }
    set radius(x)
    {
        return this._radius = x;
    }

    luas()
    {
        return this._phi * this._radius * this._radius;
    }

    keliling()
    {
        return 2 * this._phi * this._radius;
    }
}

let ling = new Lingkaran(10);
console.log(ling.radius);
console.log(ling.luas());
console.log(ling.keliling());
