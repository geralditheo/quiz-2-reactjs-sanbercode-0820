class Persegi
{
    constructor(s)
    {
        this._sisi = s;
    }
    get sisi()
    {
        return this._sisi;
    }
    set sisi(x)
    {
        return this._sisi = x;
    }

    luas()
    {
        return this._sisi * this._sisi;
    }

    keliling()
    {
        return 4 * this._sisi;
    }
}

let segi = new Persegi(10);
console.log(segi.sisi);
console.log(segi.luas());
console.log(segi.keliling());
